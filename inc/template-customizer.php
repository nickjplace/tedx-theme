<?php

/*
==================================================================
THEME CUSTOMIZER
Defines an array of options that will be saved as 'theme_mod'
settings in your options table.
==================================================================
*/

add_action('customize_register', 'sfpaper_customizer');
function sfpaper_customizer($wp_customize) {
global $wp_customize;

/* FONT OPTIONS */
global $sfpaper_fontchoices;
/* Websafe Fonts */ $fontchoices = array( 'Helvetica Neue' => 'Helvetica Neue', 'Arial' => 'Arial', 'Lucida Bright' => 'Lucida Bright', 'Georgia' => 'Georgia', 'Times New Roman' => 'Times New Roman', /* Google Fonts */ 'Abel' => 'Abel', 'Amaranth' => 'Amaranth', 'Amatic+SC' => 'Amatic SC', 'Anonymous+Pro' => 'Anonymous Pro', 'Anton' => 'Anton', 'Architects+Daughter' => 'Architects Daughter', 'Arimo' => 'Arimo', 'Arvo' => 'Arvo', 'Asap' => 'Asap', 'Bitter' => 'Bitter', 'Black+Ops+One' => 'Black Ops One', 'Bree+Serif' => 'Bree Serif', 'Cabin' => 'Cabin', 'Cabin+Condensed' => 'Cabin Condensed', 'Calligraffitti' => 'Calligraffitti', 'Cantarell' => 'Cantarell', 'Changa+One' => 'Changa One', 'Cherry+Cream+Soda' => 'Cherry Cream Soda', 'Chewy' => 'Chewy', 'Chivo' => 'Chivo', 'Coming Soon' => 'Coming Soon', 'Copse' => 'Copse', 'Covered+By+Your+Grace' => 'Covered By Your Grace', 'Crafty+Girls' => 'Crafty Girls', 'Crimson+Text' => 'Crimson Text', 'Crushed' => 'Crushed', 'Cuprum' => 'Cuprum', 'Dancing+Script' => 'Dancing Script', 'Dosis' => 'Dosis', 'Droid+Sans' => 'Droid Sans', 'Droid+Sans+Mono' => 'Droid Sans Mono', 'Droid+Serif' => 'Droid Serif', 'Exo' => 'Exo', 'Francois+One' => 'Francois One', 'Fredoka+One' => 'Fredoka One', 'Gloria+Hallelujah' => 'Gloria Hallelujah', 'Goudy+Bookletter+1911' => 'Goudy Bookletter 1911', 'Happy+Monkey' => 'Happy Monkey', 'Homemade+Apple' => 'Homemade Apple', 'Istok+Web' => 'Istok Web', 'Josefin+Sans' => 'Josephin Sans', 'Josefin+Slab' => 'Josefin Slab', 'Judson' => 'Judson', 'Just+Me+Again+Down+Here' => 'Just Me Again Down Here', 'Kreon' => 'Kreon', 'Lora' => 'Lora', 'Lato' => 'Lato', 'Limelight' => 'Limelight', 'Lobster' => 'Lobster', 'Luckiest+Guy' => 'Luckiest Guy', 'Marvel' => 'Marvel', 'Maven+Pro' => 'Maven Pro', 'Merriweather' => 'Merriweather', 'Metamorphous' => 'Metamorphous', 'Molengo' => 'Molengo', 'Muli' => 'Muli', 'News+Cycle' => 'News Cycle', 'Nobile' => 'Nobile', 'Nothing+You+Could+Do' => 'Nothing You Could Do', 'Nunito' => 'Nunito', 'Open+Sans' => 'Open Sans', 'Open+Sans' => 'Open Sans', 'Oswald' => 'Oswald', 'Pacifico' => 'Pacifico', 'Paytone+One' => 'Paytone One', 'Permanent+Marker' => 'Permanent Marker', 'Philosopher' => 'Philosopher', 'Play' => 'Play', 'Pontano+Sans' => 'Pontano Sans', 'PT+Sans' => 'PT Sans', 'PT+Sans+Narrow' => 'PT Sans Narrow', 'PT+Sans+Caption' => 'PT Sans Caption', 'PT+Serif' => 'PT Serif', 'Questrial' => 'Questrial', 'Quicksand' => 'Quicksand', 'Raleway' => 'Raleway', 'Reenie+Beanie' => 'Reenie Beanie', 'Righteous' => 'Righteous', 'Rock+Salt' => 'Rock Salt', 'Rokkitt' => 'Rokkitt', 'Shadows+Into+Light' => 'Shadows Into Light', 'Signika' => 'Signika', 'Source+Sans+Pro' => 'Source Sans Pro', 'Squada+One' => 'Squada One', 'Sunshiney' => 'Sunshiney', 'Syncopate' => 'Syncopate', 'Tangerine' => 'Tangerine', 'The+Girl+Next+Door' => 'The Girl Next Door', 'Ubuntu' => 'Ubuntu', 'Ubuntu+Condensed' => 'Ubuntu Condensed', 'Unkempt' => 'Unkempt', 'Vollkorn' => 'Vollkorn', 'Voltaire' => 'Voltaire', 'Walter+Turncoat' => 'Walter Turncoat', 'Yanone+Kaffeesatz' => 'Yanone Kaffeesatz', );

/* FONT SIZES */
global $sfpaper_fontsizes;
$sfpaper_fontsizes = array(
			'13' => '13px (default)',
			'11' => '11px',
			'12' => '12px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			);
/* LINE HEIGHTS */
global $sfpaper_lineheights;
$sfpaper_lineheights = array('1.4' => '1.4 (default)','1.5' => '1.5','1.6' => '1.6','1.7' => '1.7','1.8' => '1.8','1.9' => '1.9','2' => '2',);

	do_action('sfpaper_add_to_customizer');

} //END OF sfpaper_customizer
/*
==================================================================
NOW WE REGISTER ALL THE CORE THEME CUSTOMIZER OPTIONS AND ADD THEM
USING THE sfpaper_add_to_customizer ACTION HOOK. WE DO THIS SO
THAT THEY CAN BE EASILY REMOVED BY DEVELOPERS. ALSO, IF YOU WANT
TO REGISTER YOUR OWN, SIMPLY COPY ANY OF THE SECTIONS BELOW INTO
YOUR OWN THEME OR PLUGIN AND EDIT FOR YOUR NEEDS. 
==================================================================
*/
/*
==================================================================
Logo
==================================================================
*/
add_action('sfpaper_add_to_customizer','sfpaper_logo_customizer_options');
function sfpaper_logo_customizer_options($wp_customize) {
	global $wp_customize;
	global $sfpaper_fontchoices;
	
	$wp_customize->add_section( 'site_logo_settings', array(
		'title'          => 'Logo',
		'priority'       => 125,
	) );

	/* Logo Image Upload */
	$wp_customize->add_setting( 'logo_image', array(
	) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_image', array(
		'label'    => __( 'Logo Image', 'sfpaper'),
		'section'  => 'site_logo_settings',
		'settings' => 'logo_image',
	) ) );

	/* Logo font 
	$wp_customize->add_setting( 'logo_font_family', array(
	'default'        => 'Open Sans',
	) );

	$wp_customize->add_control( 'logo_font_family', array(
	'label'   => 'Logo Font Family:',
	'section' => 'site_logo_settings',
	'type'    => 'select',
	'priority'        => 25,
	'choices'    => $fontchoices,
	) );*/
}
/*
==================================================================
BACKGROUND
==================================================================
*/
add_action('sfpaper_add_to_customizer','sfpaper_background_customizer_options');
function sfpaper_background_customizer_options($wp_customize) {
	global $wp_customize;
	$wp_customize->add_section( 'site_background_settings', array(
		'title'          => 'Background',
		'priority'       => 130,
	) );

	/* Background Color */
	$wp_customize->add_setting( 'site_background_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_background_color', array(
		'label'   => 'Background Color',
		'section' => 'site_background_settings',
		'settings'   => 'site_background_color',
	) ) );

	/* Background Image Upload */
	$wp_customize->add_setting( 'site_background_image', array(
	) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'site_background_image', array(
		'label'    => __( 'Background Image' , 'sfpaper'),
		'section'  => 'site_background_settings',
		'settings' => 'site_background_image',
	) ) );

	/* Background Image Repeat */
	$wp_customize->add_setting( 'site_background_repeat', array(
		'default'        => 'repeat',
	) );
	
	$wp_customize->add_control( 'site_background_repeat', array(
		'label'   => 'Background Image Repeat:',
		'section' => 'site_background_settings',
		'type'    => 'select',
		'choices'    => array(
			'repeat' => 'Repeat',
			'repeat-x' => 'Repeat Horizontally',
			'repeat-y' => 'Repeat Vertically',
			'no-repeat' => 'No Repeat',
			),
	) );
	
	/* Background Image Position */
	$wp_customize->add_setting( 'site_background_position', array(
		'default'        => 'top center',
	) );
	
	$wp_customize->add_control( 'site_background_position', array(
		'label'   => 'Background Image Position:',
		'section' => 'site_background_settings',
		'type'    => 'select',
		'choices'    => array(
			'top center' => 'Top Center',
			'top left' => 'Top Left',
			'top right' => 'Top Right',
			'bottom center' => 'Bottom Center',
			'bottom left' => 'Bottom Left',
			'bottom right' => 'Bottom Right',
			),
	) );
	
	/* Background Image Attachment */
	$wp_customize->add_setting( 'site_background_attachment', array(
		'default'        => 'scroll',
	) );
	
	$wp_customize->add_control( 'site_background_attachment', array(
		'label'   => 'Background Image Attachment:',
		'section' => 'site_background_settings',
		'type'    => 'select',
		'choices'    => array(
			'scroll' => 'Scroll (moves with the content)',
			'fixed' => 'Fixed (remains static behind content)',
			),
	) );
}// END BACKGROUND SETTINGS

/*
==================================================================
SITE COLORS
==================================================================
*/
add_action('sfpaper_add_to_customizer','sfpaper_colors_customizer_options');
function sfpaper_colors_customizer_options($wp_customize) {
	global $wp_customize;
	$wp_customize->add_section( 'color_settings', array(
		'title'          => 'Color Scheme',
		'priority'       => 140,
		'description'    => __( 'Choose colors for your theme.', 'sfpaper' ),
	) );
	
	/* Headings Color */
	$wp_customize->add_setting( 'heading_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'heading_color', array(
		'label'   => 'Headings Color',
		'section' => 'color_settings',
		'settings'   => 'heading_color',
		'priority'       => 10,
	) ) );
	
	/* Main Text Color */
	$wp_customize->add_setting( 'body_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_color', array(
		'label'   => 'Main Text Color',
		'section' => 'color_settings',
		'settings'   => 'body_color',
		'priority'       => 20,
	) ) );

	/* Small/Meta Text Color */
	$wp_customize->add_setting( 'small_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'small_color', array(
		'label'   => 'Small/Meta Text Color',
		'section' => 'color_settings',
		'settings'   => 'small_color',
		'priority'       => 30,
	) ) );
	
	/* Link Color */
	$wp_customize->add_setting( 'link_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_color', array(
		'label'   => 'Link Color',
		'section' => 'color_settings',
		'settings'   => 'link_color',
		'priority'       => 40,
	) ) );
	
	/* Border Color */
	$wp_customize->add_setting( 'border_color', array(
		'default'        => '',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color', array(
		'label'   => 'Border Color',
		'section' => 'color_settings',
		'settings'   => 'border_color',
		'priority'       => 50,
	) ) );
}// END COLOR SETTINGS
/*
==================================================================
FOOTER
==================================================================
*/
add_action('sfpaper_add_to_customizer','sfpaper_footer_customizer_options');
function sfpaper_footer_customizer_options($wp_customize) {
	global $wp_customize;
	/* Add footer section and color styles to customizer */
	$wp_customize->add_section( 'footer_settings', array(
		'title'          => 'Footer',
		'priority'       => 150,
	) );
	
	/* Footer Credits */
	$wp_customize->add_setting( 'footer_credits', array(
		'default'        => '',
	) );
	
	$wp_customize->add_control( 'footer_credits', array(
		'label'   => 'Footer Credits',
		'section' => 'footer_settings',
		'settings'   => 'footer_credits',
		'type' => 'text',
		'priority'       => 45,
	) );
}// END FOOTER SETTINGS

/*
==========================================================
Loads the custom styles from the Theme Customizer
==========================================================
*/
add_action( 'wp_head', 'sfpaper_custom_style');
function sfpaper_custom_style() { ?>
<style>
/* BODY */
<?php
/* Set Variables */
$bg_color = get_theme_mod( 'site_background_color');
$bg_image = get_theme_mod( 'site_background_image');
$bg_repeat = get_theme_mod( 'site_background_repeat');
$bg_position = get_theme_mod( 'site_background_position');
$bg_attachment = get_theme_mod( 'site_background_attachment');
$body_size = get_theme_mod( 'body_size');
$body_font = get_theme_mod( 'body_font');
$body_color = get_theme_mod( 'body_color');
$body_line = get_theme_mod( 'body_line_height');
$logo_font = get_theme_mod( 'logo_font_family');
$heading_font = get_theme_mod( 'heading_font');
$heading_color = get_theme_mod( 'heading_color');
$small_color = get_theme_mod( 'small_color');
$link_color = get_theme_mod( 'link_color');
$border_color = get_theme_mod( 'border_color');
$heading_color = get_theme_mod( 'heading_color');

/* Site Background */
echo 'body {';
if($bg_color) {echo 'background-color:' .$bg_color.';';}
if($bg_image) {echo 'background-image:url("' .$bg_image.'");';}
if($bg_repeat) {echo 'background-repeat:' .$bg_repeat.';';}
if($bg_position) {echo 'background-position:' .$bg_position.';';}
if($bg_attachment) {echo 'background-attachment:' .$bg_attachment.';';}
echo '}';

/* Main Text Typography */
echo 'body {';
if($body_font){echo 'font-family:"' .$body_font.'",helvetica,arial,sans-serif;';}
if($body_color){echo 'color:' .$body_color.';';}
if($body_size){echo 'font-size:'.$body_size.'px;';}
if($body_line){echo 'line-height:' .$body_line.';';}
echo '}';
if($body_line){echo 'h4, h5, h6 {line-height:' .$body_line.';}';}

/* Headings Typography */
echo 'h1,h2,h3 {';
if($heading_font){echo 'font-family:"' .$heading_font.'",helvetica,arial,sans-serif;';}
if($heading_color){echo 'color:' .$heading_color.';';}
echo'}';

/* Small Color */
if($small_color){echo 'h1 small, h2 small, h3 small, h4 small, h5 small, h6 small, blockquote small, .entry-meta {color:' .$small_color.';}';}

/* Link Color */
if($link_color){echo 'a,.site-title a:hover,.main-navigation a:hover {color:'.$link_color.';} .main-navigation .sub-menu a:hover,.main-navigation ul.children a:hover{background:'.$link_color.'}@media (max-width:768px) {.menu a:hover {text-decoration:none;background:'.$link_color.';}}';}

/* Border Color */
if($border_color){echo '.hentry,.wp-caption,#main,.page .hentry,.widget li,#colophon,blockquote,input[type="text"], input[type="email"], input[type="password"], textarea,.nav-previous a,.nav-next a,a.more-link {border-color:' .$border_color.';}';}

do_action('sfpaper_add_to_custom_style');

?>
</style>
<?php }
/*
==========================================================
GOOGLE FONTS
==========================================================
*/
add_action('sfpaper_head', 'sfpaper_add_google_fonts', 5);
function sfpaper_add_google_fonts() {
	$googlefonts = false;
	$webfonts = array('Helvetica Neue','Georgia','Lucida Bright','Arial','Times New Roman');
	
	$logofont = get_theme_mod('logo_font_family', 'Helvetica Neue');
	$bodyfont = get_theme_mod('body_font_family', 'Helvetica Neue');
	$headingfont = get_theme_mod('heading_font_family', 'Helvetica Neue');
	
	if (!in_array($logofont, $webfonts)) {$googlefonts .= $logofont.'|';}
	if (!in_array($bodyfont, $webfonts)) {$googlefonts .= $bodyfont.'|';}
	if (!in_array($headingfont, $webfonts)) {$googlefonts .= $headingfont.'|';}
	
	if(!$googlefonts == false) {
		echo '<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='.$googlefonts.'" media="screen">';
	}
}
